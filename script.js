// variable defining
const steps = document.querySelectorAll(".stp");
const circles = document.querySelectorAll(".circle");
const formInputs = document.querySelectorAll(".step-1 form input");
const nextButton = document.querySelectorAll(".next-stp");
const backButton = document.querySelectorAll(".prev-stp");
let selection = false;
let serviceName = document.querySelectorAll(".service-name");
let servicePrice = document.querySelectorAll(".servic-price");
let detailedExtra = [
  "Online Service",
  "Larger Storage",
  "Customizable Profile",
];
let totalPrice = document.querySelector(".total");
let cost = document.getElementById("totalPrice");
const mainPlan = document.querySelector(".plan-name");
const mainPrice = document.querySelector(".plan-price");
const extraSelect = [0, 0, 0];
const mon = ["+$1/mo", "+$2/mo", "+$2/mo"];
const year = ["+$10/yr", "+$20/yr", "+$20/yr"];
circles[0].style.backgroundColor = "hsl(0, 0%, 100%)";
circles[0].style.color = "black";
let extraAddons = [];
let monp = [1, 2, 2];
let yrp = [10, 20, 20];
function formsError(el) {
  const idVal = el.id;
  const labels = document.getElementsByTagName("label");
  for (let i = 0; i < labels.length; i++) {
    if (labels[i].htmlFor == idVal) {
      return labels[i];
    }
  }
}

function validFormInputs() {
  // name check start
  let name = formInputs[0].value;
  name = name.trim();
  const errors = document.querySelectorAll(".error");
  const outline = document.querySelectorAll("input");
  if (name === "") {
    errors[0].style.display = "flex";
    outline[0].style.borderColor = "red";
    return false;
  }

  for (let i = 0; i < name.length; i++) {
    const char = name[i];
    if (
      !(
        (char >= "a" && char <= "z") ||
        (char >= "A" && char <= "Z") ||
        char === " " ||
        char === "-"
      )
    ) {
      errors[0].style.display = "flex";
      outline[0].style.borderColor = "red";
      return false;
    }
  }

  for (let i = 1; i < name.length; i++) {
    if (
      (name[i] === " " || name[i] === "-") &&
      (name[i - 1] === " " || name[i - 1] === "-")
    ) {
      errors[0].style.display = "flex";
      outline[0].style.borderColor = "red";
      return false;
    }
  }
  // name check end
  // email check start
  let email = formInputs[1].value;
  if (email.indexOf("@") === -1 || email.indexOf(".") === -1) {
    errors[1].style.display = "flex";
    outline[1].style.borderColor = "red";
    return false;
  }

  const [username, domain] = email.split("@");

  if (username.length === 0 || domain.length === 0) {
    errors[1].style.display = "flex";
    outline[1].style.borderColor = "red";
    return false;
  }

  if (domain.indexOf(".") === -1) {
    errors[1].style.display = "flex";
    outline[1].style.borderColor = "red";
    return false;
  }

  if (domain.includes("..")) {
    errors[1].style.display = "flex";
    outline[1].style.borderColor = "red";
    return false;
  }

  if (
    email[0] === "." ||
    email[0] === "@" ||
    email[email.length - 1] === "." ||
    email[email.length - 1] === "@"
  ) {
    errors[1].style.display = "flex";
    outline[1].style.borderColor = "red";
    return false;
  }

  if (email.indexOf(" ") !== -1) {
    errors[1].style.display = "flex";
    outline[1].style.borderColor = "red";
    return false;
  }
  //number check start
  let phone = formInputs[2].value;
  if (phone.length === 0) {
    errors[2].style.display = "flex";
    outline[2].style.borderColor = "red";
    return false;
  }
  for (let i = 0; i < phone.length; i++) {
    if (isNaN(parseInt(phone[i]))) {
      errors[2].style.display = "flex";
      outline[2].style.borderColor = "red";
      return false;
    }
  }
  //number check end
  return true;
}
let sum = 0;
function iterateStep(boolean) {
  if (boolean) {
    for (let currentStep = 0; currentStep < steps.length; currentStep++) {
      if (window.getComputedStyle(steps[currentStep]).display === "flex") {
        steps[currentStep].style.display = "none";

        if (currentStep < 3) {
          circles[currentStep].style.backgroundColor = "transparent";
          circles[currentStep].style.color = "white";
        }
        if (currentStep + 1 === 3) {
          extraSelect.forEach((check, index) => {
            if (check === 1) {
              serviceName[index].textContent = detailedExtra[index];
              if (selection) {
                sum += yrp[index];
                servicePrice[index].textContent = year[index];
              } else {
                sum += monp[index];
                servicePrice[index].textContent = mon[index];
              }
            } else {
              serviceName[index].textContent = "";
              if (selection) {
                sum -= yrp[index];
              } else {
                sum -= monp[index];
              }
              servicePrice[index].textContent = "";
            }
          });
        }
        steps[currentStep + 1].style.display = "flex";

        if (currentStep < 3) {
          circles[currentStep + 1].style.backgroundColor = "white";
          circles[currentStep + 1].style.color = "black";
        }

        return;
      }
    }
  } else {
    for (let currentStep = 0; currentStep < steps.length; currentStep++) {
      if (window.getComputedStyle(steps[currentStep]).display === "flex") {
        steps[currentStep].style.display = "none";
        circles[currentStep].style.backgroundColor = "transparent";
        circles[currentStep].style.color = "white";
        steps[currentStep - 1].style.display = "flex";

        circles[currentStep - 1].style.backgroundColor = "white";
        circles[currentStep - 1].style.color = "black";

        return;
      }
    }
  }
}
// nextbutton by page
nextButton.forEach((button) => {
  button.addEventListener("click", () => {
    if (validFormInputs()) {
      iterateStep(1);
    }
    extrasAdd(selection);
    updateSummary(planName, planPrice, selection);
  });
});
//backbutton by page
backButton.forEach((button) => {
  button.addEventListener("click", () => {
    iterateStep(0);
  });
});

const toggle = document.querySelector(".switch");

//2nd page price update
function priceUpdate(checked) {
  const yearlyPrice = [90, 120, 150];
  const monthlyPrice = [9, 12, 15];
  const prices = document.querySelectorAll(".plan-priced");
  const year = "2 months free";
  if (checked) {
    prices[0].textContent = `$ ${yearlyPrice[0]}/yr`;
    prices[0].appendChild(document.createElement("br"));
    const yearlyfreeElement = document.createElement("span");
    yearlyfreeElement.textContent = `${year}`;
    prices[0].appendChild(yearlyfreeElement);
    yearlyfreeElement.classList.add("freeStuff");
    yearlyfreeElement.style.color = "black";
    yearlyfreeElement.style.fontSize = "12px";
    prices[1].textContent = `$${yearlyPrice[1]}/yr`;
    prices[1].appendChild(document.createElement("br"));

    const yearlyfreeElement1 = document.createElement("span");
    yearlyfreeElement1.textContent = `${year}`;
    prices[1].appendChild(yearlyfreeElement1);
    yearlyfreeElement1.style.color = "black";
    yearlyfreeElement1.style.fontSize = "12px";
    prices[2].textContent = `$${yearlyPrice[2]}/yr`;
    prices[2].appendChild(document.createElement("br"));
    const yearlyfreeElement2 = document.createElement("span");
    yearlyfreeElement2.textContent = `${year}`;
    prices[2].appendChild(yearlyfreeElement2);
    yearlyfreeElement2.style.color = "black";
    yearlyfreeElement2.style.fontSize = "12px";
  } else {
    prices[0].textContent = `$ ${monthlyPrice[0]}/mo`;
    prices[1].textContent = `$${monthlyPrice[1]}/mo`;
    prices[2].textContent = `$${monthlyPrice[2]}/mo`;
  }
}
const free = document.querySelectorAll(".free");
//toggle switching
toggle.addEventListener("click", () => {
  const value = toggle.querySelector("input").checked;
  if (value) {
    document.querySelector(".monthly").classList.remove("sw-active");
    document.querySelector(".yearly").classList.add("sw-active");

    free.forEach((fr) => {
      fr.textContent = "2 months free";
    });
  } else {
    document.querySelector(".monthly").classList.add("sw-active");
    document.querySelector(".yearly").classList.remove("sw-active");
  }
  priceUpdate(value);
  selection = value;
});
let planName = "";
let planPrice = "";
//plan selection
const plans = document.querySelectorAll(".plan-card");
plans.forEach((plan) => {
  plan.addEventListener("click", () => {
    document.querySelector(".selected").classList.remove("selected");
    plan.classList.add("selected");
    planName = plan.querySelector("b").textContent;
    planPrice = plan.querySelector(".plan-priced").textContent;
  });
});

const extras = document.querySelectorAll(".price");

// extras reflection
function extrasAdd(selection) {
  if (selection) {
    extras[0].textContent = year[0];
    extras[1].textContent = year[1];
    extras[2].textContent = year[2];
  } else {
    extras[0].textContent = mon[0];
    extras[1].textContent = mon[1];
    extras[2].textContent = mon[2];
  }
}

// updating main summary
function updateSummary(name, price, bool) {
  if (bool) {
    if (name.length === 0) {
      name = "Arcade(Yearly)";
      price = "$ 90/yr ";
    } else {
      name += "(Yearly)";
    }
    mainPlan.textContent = name;
    mainPrice.textContent = price.slice(0, 7);
  } else {
    if (name.length === 0) {
      name = "Arcade(Monthly)";
      price = "$ 9/mo ";
    } else {
      name += "(Monthly)";
    }
    mainPlan.textContent = name;
    mainPrice.textContent = price.slice(0, 6);
  }
  return price;
}

// change button
const undo = document.querySelector(".undo");
undo.addEventListener("click", () => {
  steps[3].style.display = "none";
  steps[1].style.display = "flex";
});

const checkBox = [];
checkBox.push(document.querySelector("#online"));
checkBox.push(document.querySelector("#larger"));
checkBox.push(document.querySelector("#profile"));
checkBox.forEach((box, index) => {
  box.addEventListener("click", () => {
    extraSelect[index] = extraSelect[index] ^ 1;
  });
});
//extrasCatcherUpdater
